$(document).on('ready',function(){
	$('#tweets').tweets({
		tweets:3,
		username: "graficoprisma"
	});
	$('#map').gmap3({
		action:'init',
		options:{
			center:[20.65281, -105.21545],
			zoom:17,
			mapTypeControl: false,
			navigationControl: true,
			scrollwheel: true,
			streetViewControl: false
		}
	});

	$('#map').gmap3({
		action: 'addMarker',
		lat: 20.651951,
  		lng: -105.215469,
		marker:{
			options:{
				draggable:false,
				icon:"/images/icon.png"
			}
		},
		infowindow:{
			options:{
				content:'<h3>Imprenta Prisma</h3><ul class="info"><li>20 de Noviembre 307-A Loc. 1</li><li>Pitillal, Jalisco</li><li><span class="icon">8</span> +523222228368</li></ul>'
			}
		}
	});

	$('#slider').bjqs({
		height:320,
		width:960,
		responsive: true,
		nexttext : '}',
		prevtext : '{',
		centercontrols : true,
		showmarkers : true,
		centermarkers : true
	});
});